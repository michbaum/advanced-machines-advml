import os
import typing
import numpy as np
import xgboost as xgb
import pandas as pd
import csv
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import f1_score
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, RepeatedKFold, train_test_split
from hrvanalysis import remove_outliers, remove_ectopic_beats, interpolate_nan_values
import biosppy.signals.ecg as ecg
from hrvanalysis import get_time_domain_features
from hrvanalysis import get_frequency_domain_features
import neurokit2 as nk
from sklearn.impute import SimpleImputer
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import mutual_info_classif
import pywt
from tqdm import tqdm
import heartpy
from collections import Counter
import scipy
from neurokit2.signal import signal_rate, signal_sanitize
from neurokit2 import ecg_clean
from neurokit2 import ecg_delineate
from neurokit2 import ecg_peaks
from neurokit2 import ecg_phase
from neurokit2 import ecg_quality
from sklearn.preprocessing import normalize
from sklearn.utils.class_weight import compute_sample_weight


DEBUG = False
VERBOSE = True
LOAD_FEATURES = True
CONTINUE_LOADING_FEATURES = False
GRIDSEARCH = False


class ECGClassificationModel(object):
    '''
    Gradient Boosted Decision Tree model for classification.
    '''

    def __init__(self, modelname="XGB") -> None:
        '''
        Initialize the model.
        '''
        # Seed RNG for reproducibility
        self.seed = 42
        self.rng = np.random.default_rng(seed=self.seed)

        self.modelname = modelname
        # How many different parameters our randomized search will initialize
        self.num_param_samples = 10
        # self.feature_set = np.empty()

        # TODO: Change this number according to the final number of features we have
        self.k_features = 100

        # Instantiation
        # 
        if self.modelname == "XGB":
            self.model = xgb.XGBClassifier(
                n_estimators=10000,
                max_depth=5,
                learning_rate=0.03,
                random_state=self.seed,
                n_jobs=-1,
                subsample=0.5,
                objective='multi:softmax',
                num_class=4,
                eval_metric='mlogloss',
                use_label_encoder=False
            )

        # Tuning parameters

        # For model fitting
        self.k_splits = 5
        self.k_repeats = 3


    def get_param_grid(self):
        # TODO: Change parameters for classification model
        if self.modelname == "XGB":
            param_grid = {
                'n_estimators': range(500, 4000, 500),  # range(140, 201, 5),
                'max_depth': range(3, 7, 1),  # range(4, 6, 1),
                'learning_rate': np.arange(0.02, 0.06, 0.01),
                'random_state': [self.seed],
                'n_jobs': [-1],
                'subsample': np.arange(0.5, 1.01, 0.1),
                'objective': ['multi:softmax'],
                'num_class': [4],
                'sample_weight': [[1/3030, 1/443, 1/1474, 1/170]],
                'eval_metric': ['mlogloss'],
                'use_label_encoder': [False]
            }
            # TODO: Add the class inbalance thingy
        
        return param_grid

    def fit_model(self, train_features, train_GT) -> None:
        '''
        Fit the model to the data
        '''
        # [1/3030, 1/443, 1/1474, 1/170]
        # sample_weights = [[1/3030, 1/443, 1/1474, 1/170][y] for y in train_GT.ravel()]
        # vprint(sample_weights[-6:])
        self.model.fit(train_features, train_GT, sample_weight=compute_sample_weight(class_weight='balanced', y=train_GT.ravel()))
        # compute_sample_weight(class_weight='balanced', y=train_GT.ravel())
        # self.model.fit(train_features, train_GT)

    def predict(self, test_features) -> np.ndarray:
        '''
        Predict the age of the patient based on the MRI features
        '''
        return self.model.predict(test_features)

    def accuracy(self, y_pred, y_true):
        return f1_score(y_true, y_pred, average='micro')





    def extract_statistical_features(self, time_domain_data, feature_set):
        """
        This method returns a matrix of statistical
        features such as mean, median and std of the signal.
        """
        # TODO: Don't know, if these features really hold any useful information
        # The mean/median could be somewhat useful, but at the same time I think
        # we'll want to eliminate absolute differences there
        # The std is not useful whatsoever though?
        mean = [np.nanmean(x) for x in time_domain_data]
        median = [np.nanmedian(x) for x in time_domain_data]
        std = [np.nanstd(x) for x in time_domain_data]

        mean = np.array(mean).reshape(-1,1)
        median = np.array(median).reshape(-1,1)
        std = np.array(std).reshape(-1,1)

        statistics = np.concatenate((mean, median, std), axis=1)
        return np.concatenate((feature_set, statistics), axis=1)
    


    def extract_time_domain_features(self, time_domain_data, feature_set):
        # TODO: Remove baseline wandering? (influences peak height of patients differently?)
        # TODO: Add medians as well?
        # TODO: Scale signal with R peak median?
        # TODO: Duration of heartbeat?

        P_mean = []
        P_std =  []
        P_min = []
        P_max = []
        P_median = []
        Q_mean = []
        Q_std =  []
        Q_min = []
        Q_max = []
        Q_median = []
        R_mean = []
        R_std =  []
        R_min = []
        R_max =[]
        R_median = []
        S_mean = []
        S_std =  []
        S_min = []
        S_max = []
        S_median = []

        P_dur_mean = []
        P_dur_std = []
        P_dur_min = []
        P_dur_max = []
        P_dur_median = []
        T_dur_mean = []
        T_dur_std = []
        T_dur_min = []
        T_dur_max = []
        T_dur_median = []
        R_dur_mean = []
        R_dur_std = []
        R_dur_min = []
        R_dur_max = []
        R_dur_median = []

        QR_occurence_ratio = []
        RS_occurence_ratio = []

        #heartrate_mean = []
        #heartrate_std = []

        QR_ratio = []
        SR_ratio = []

        signal_q = []


        for patient in tqdm(time_domain_data):
            # Drop the NaN values
            patient = patient[~np.isnan(patient)]
            # processed = nk.ecg_process(patient, sampling_rate=300)[0]
            #--------------------------------------------------------------dirty fix--------------------------------------------------------------
            ecg_signal = patient
            sampling_rate=300
            method="neurokit"


            ecg_signal = signal_sanitize(ecg_signal)

            ecg_cleaned = ecg_clean(ecg_signal, sampling_rate=sampling_rate, method=method)
            # R-peaks
            instant_peaks, rpeaks, = ecg_peaks(
                ecg_cleaned=ecg_cleaned, sampling_rate=sampling_rate, method=method, correct_artifacts=True
            )

            rate = signal_rate(rpeaks, sampling_rate=sampling_rate, desired_length=len(ecg_cleaned))

            quality = ecg_quality(ecg_cleaned, rpeaks=None, sampling_rate=sampling_rate)

            signals = pd.DataFrame({"ECG_Raw": ecg_signal, "ECG_Clean": ecg_cleaned, "ECG_Rate": rate, "ECG_Quality": quality})

            # Additional info of the ecg signal
            delineate_signal, delineate_info = ecg_delineate(
                ecg_cleaned=ecg_cleaned, rpeaks=None, sampling_rate=sampling_rate
            )

            cardiac_phase = ecg_phase(ecg_cleaned=ecg_cleaned, rpeaks=None, delineate_info=delineate_info, sampling_rate=300)

            signals = pd.concat([signals, instant_peaks, delineate_signal, cardiac_phase], axis=1)

            # Rpeaks location and sampling rate in dict info
            info = rpeaks
            info['sampling_rate'] = sampling_rate

            processed = signals
            #-----------------------------------------------------------------------------------------------------------------
            #TODO: Check for further features. + more durations?`


            #peaks
            Q_peaks = processed["ECG_Q_Peaks"]
            Q_values = []
            for j in range(0,len(Q_peaks)):
                if Q_peaks[j]==1:
                    Q_values.append(patient[j])

            R_peaks = processed["ECG_R_Peaks"]
            # TODO: Drop outlier R peaks?
            R_values = []
            for j in range(0,len(R_peaks)):
                if R_peaks[j]==1:
                    R_values.append(patient[j])

            P_peaks = processed["ECG_P_Peaks"]
            P_values = []
            for j in range(0,len(P_peaks)):
                if P_peaks[j]==1:
                    P_values.append(patient[j])

            S_peaks = processed["ECG_S_Peaks"]
            S_values = []
            for j in range(0,len(S_peaks)):
                if S_peaks[j]==1:
                    S_values.append(patient[j])

            #on-and offsets    
            P_onsets = processed['ECG_P_Onsets']
            P_offsets = processed['ECG_P_Offsets']
            T_onsets = processed['ECG_T_Onsets']
            T_offsets = processed['ECG_T_Offsets']
            R_onsets = processed['ECG_R_Onsets']
            R_offsets = processed['ECG_R_Offsets']

            #time diffs
            P_durations = []
            for j in range(0, len(P_onsets)):
                if P_onsets[j]==1:
                    for i in range(j, len(P_offsets)):
                        if P_offsets[i]==1:
                            P_durations.append(i-j)
                            continue

            T_durations = []
            for j in range(0, len(T_onsets)):
                if T_onsets[j]==1:
                    for i in range(j, len(T_offsets)):
                        if T_offsets[i]==1:
                            T_durations.append(i-j)
                            continue

            R_durations = []
            for j in range(0, len(R_onsets)):
                if R_onsets[j]==1:
                    for i in range(j, len(R_offsets)):
                        if R_offsets[i]==1:
                            R_durations.append(i-j)
                            continue

            #TODO: QRS Duration? Need to be careful, asnot always e.g. Q present.
            #Misc
            heartrate = processed["ECG_Rate"]
            #Means and stds
            Q_mean.append(np.nanmean(Q_values))
            Q_std.append(np.nanstd(Q_values))
            if Q_values != []:
                Q_min.append(np.nanmin(Q_values))
                Q_max.append(np.nanmax(Q_values))
                Q_median.append(np.nanmedian(Q_values))
            else:
                Q_min.append(np.nan)
                Q_max.append(np.nan)
                Q_median.append(np.nan)
            
            R_mean.append(np.nanmean(R_values))
            R_std.append(np.nanstd(R_values))
            if R_values != []:
                R_min.append(np.nanmin(R_values))
                R_max.append(np.nanmax(R_values))
                R_median.append(np.nanmedian(R_values))
            else:
                R_min.append(np.nan)
                R_max.append(np.nan)
                R_median.append(np.nan)


            P_mean.append(np.nanmean(P_values))
            P_std.append(np.nanstd(P_values))
            if P_values != []:
                P_min.append(np.nanmin(P_values))
                P_max.append(np.nanmax(P_values))
                P_median.append(np.nanmedian(P_values))
            else:
                P_min.append(np.nan)
                P_max.append(np.nan)
                P_median.append(np.nan)

            S_mean.append(np.nanmean(S_values))
            S_std.append(np.nanstd(S_values))
            if S_values != []:
                S_min.append(np.nanmin(S_values))
                S_max.append(np.nanmax(S_values))
                S_median.append(np.nanmedian(S_values))
            else:
                S_min.append(np.nan)
                S_max.append(np.nan)
                S_median.append(np.nan)


            P_dur_mean.append(np.nanmean(P_durations))
            P_dur_std.append(np.nanstd(P_durations))
            if P_durations != []:
                P_dur_min.append(np.nanmin(P_durations))
                P_dur_max.append(np.nanmax(P_durations))
                P_dur_median.append(np.nanmedian(P_durations))
            else:
                P_dur_min.append(np.nan)
                P_dur_max.append(np.nan)
                P_dur_median.append(np.nan)
            

            T_dur_mean.append(np.nanmean(T_durations))
            T_dur_std.append(np.nanstd(T_durations))
            if T_durations != []:
                T_dur_min.append(np.nanmin(T_durations))
                T_dur_max.append(np.nanmax(T_durations))
                T_dur_median.append(np.nanmedian(T_durations))
            else:
                T_dur_min.append(np.nan)
                T_dur_max.append(np.nan)
                T_dur_median.append(np.nan)


            R_dur_mean.append(np.nanmean(R_durations))
            R_dur_std.append(np.nanstd(R_durations))
            if R_durations != []:
                R_dur_min.append(np.nanmin(R_durations))
                R_dur_max.append(np.nanmax(R_durations))
                R_dur_median.append(np.nanmedian(R_durations))
            else:
                R_dur_min.append(np.nan)
                R_dur_max.append(np.nan)
                R_dur_median.append(np.nan)

            if len(R_values) == 0:
                QR_occurence_ratio.append(np.nan)
                RS_occurence_ratio.append(np.nan)
            else:
                QR_occurence_ratio.append((len(Q_values))/(len(R_values)))
                RS_occurence_ratio.append((len(S_values))/(len(R_values)))

            
            QR_ratio.append((np.nanmean(Q_values))/(np.nanmean(R_values)))
            SR_ratio.append((np.nanmean(S_values))/(np.nanmean(R_values)))

            ecg_cleaned = processed["ECG_Clean"]

            signal_q.append(nk.ecg_quality(ecg_cleaned=ecg_cleaned, sampling_rate=300))

            

            # heartrate_mean.append(np.nanmean(heartrate))
            # heartrate_std.append(np.nanstd(heartrate))

        features = np.concatenate((np.array(Q_mean).reshape(-1,1), np.array(Q_std).reshape(-1,1), np.array(R_mean).reshape(-1,1), np.array(R_std).reshape(-1,1),
        np.array(P_mean).reshape(-1,1), np.array(P_std).reshape(-1,1), np.array(S_mean).reshape(-1,1), np.array(S_std).reshape(-1,1), np.array(P_dur_mean).reshape(-1,1),
        np.array(P_dur_std).reshape(-1,1), np.array(T_dur_mean).reshape(-1,1),np.array(T_dur_std).reshape(-1,1),np.array(R_dur_mean).reshape(-1,1),np.array(R_dur_std).reshape(-1,1),
        np.array(QR_occurence_ratio).reshape(-1,1), np.array(RS_occurence_ratio).reshape(-1,1), np.array(QR_ratio).reshape(-1,1), np.array(SR_ratio).reshape(-1,1),
        np.array(signal_q).reshape(-1,1),np.array(Q_min).reshape(-1,1), np.array(Q_max).reshape(-1,1), np.array(Q_median).reshape(-1,1),np.array(R_min).reshape(-1,1), np.array(R_max).reshape(-1,1), np.array(R_median).reshape(-1,1),
        np.array(P_min).reshape(-1,1), np.array(P_max).reshape(-1,1), np.array(P_median).reshape(-1,1),np.array(S_min).reshape(-1,1), np.array(S_max).reshape(-1,1), np.array(S_median).reshape(-1,1),
        np.array(P_dur_min).reshape(-1,1), np.array(P_dur_max).reshape(-1,1), np.array(P_dur_median).reshape(-1,1),np.array(T_dur_min).reshape(-1,1), np.array(T_dur_max).reshape(-1,1), np.array(T_dur_median).reshape(-1,1),
        np.array(R_dur_min).reshape(-1,1), np.array(R_dur_max).reshape(-1,1), np.array(R_dur_median).reshape(-1,1)), axis=1)

        np.savetxt('extract_time_domain_features.csv', features,delimiter=',')


        return np.concatenate((feature_set, features), axis=1)
    
    def get_ecg_signals(self, time_domain_data):
        """
        This method extracs ecg data for each patient and stores in a list
        """
        ts_list = []
        filtered_list = []
        rpeaks_list = []
        templates_ts_list = []
        templates_list = []
        heart_rate_ts_list = []
        heart_rate_list = []

        for r in tqdm(range(0, len(time_domain_data))):
            rvec = time_domain_data[r]
            # drop nans
            rvec = rvec[~np.isnan(rvec)]
            rvec = rvec.astype(int)

            # TODO: Double check if it makes sense to filter our signal once again, see python notebook, they didn't use ecg
            ts, filtered, rpeaks, templates_ts, templates, heart_rate_ts, heart_rate = ecg.ecg(
                signal=rvec, sampling_rate = 300, show = False)
            
            ts_list.append(ts)
            filtered_list.append(filtered)
            rpeaks_list.append(rpeaks)
            templates_ts_list.append(templates_ts)
            templates_list.append(templates)
            heart_rate_ts_list.append(heart_rate_ts)
            heart_rate_list.append(heart_rate)
        
        return ts_list, filtered_list, rpeaks_list, templates_ts_list, templates_list, heart_rate_ts_list, heart_rate_list

    
    def get_heartbeat(self, templates):
        # Templates = [[[template1], [template2], ...], [[template1], [template2], ...], ...]
        # i = [[template1], [template2], ...]
        templ_normalized = [normalize(i) for i in templates]
        # i = [[template1], [template2], ...]
        heart_beats = [np.mean(i, axis=0) for i in templ_normalized]
        # heart_beats = [[template_patient1], [template_patient2], ...]
        return heart_beats
    
    def get_heartrates(self, heartrates, feature_set):
        mean = [np.nanmean(i) for i in heartrates]
        std = [np.nanstd(i) for i in heartrates]

        median = []
        for pat_rate in heartrates:
            if pat_rate != []:
                median.append(np.nanmedian(pat_rate))
            else:
                median.append(np.nan)
        mean = np.array(mean).reshape(-1,1)
        std = np.array(std).reshape(-1,1)
        median = np.array(median).reshape(-1,1)
        data = np.concatenate((mean, median, std), axis = 1)
        feature_set = np.concatenate((feature_set, data), axis = 1)
        return feature_set
        
    def extract_heartbeat_statistical_features(self, heartbeats, feature_set):
        """
        Returns matrix of statistical feeatures of the heartbeats
        """
        max_amp = [max(i) for i in heartbeats]
        min_amp = [min(i) for i in heartbeats]
        mean_amp = [np.mean(i) for i in heartbeats]
        med_amp = [np.median(i) for i in heartbeats]
        std_amp = [np.std(i) for i in heartbeats]
        
        max_amp = np.array(max_amp).reshape(-1,1)
        min_amp = np.array(min_amp).reshape(-1,1)
        mean_amp = np.array(mean_amp).reshape(-1,1)
        med_amp = np.array(med_amp).reshape(-1,1)
        std_amp = np.array(std_amp).reshape(-1,1)

        feature_set = np.concatenate((feature_set, max_amp, min_amp, mean_amp, med_amp, std_amp), axis = 1)
        return feature_set

    def extract_peak_features(self, heartbeats):
        R_index = []
        R_value = []
        Q_index = []
        Q_value = []
        P_index = []
        P_value = []
        S_index = []
        S_value = []
        T_index = []
        T_value = []

        for curr_beat in tqdm(heartbeats):
            r_index = np.where(curr_beat == max(curr_beat))[0][0]
            R_index.append(r_index)
            r_val = curr_beat[r_index]
            R_value.append(r_val)
            
            Q_area = curr_beat[:r_index]
            q_index = np.where(curr_beat == min(Q_area))[0][0]
            Q_index.append(q_index)
            q_val = curr_beat[q_index]
            Q_value.append(q_val)
            
            P_area = curr_beat[:q_index+1]
            p_index = np.where(curr_beat == max(P_area))[0][0]
            P_index.append(p_index)
            p_val = curr_beat[p_index]
            P_value.append(p_val)
            
            S_area = curr_beat[r_index:]
            s_index = np.where(curr_beat == min(S_area))[0][0]
            S_index.append(s_index)
            s_val = curr_beat[s_index]
            S_value.append(s_val)
            
            T_area = curr_beat[s_index:]
            t_index = np.where(curr_beat == max(T_area))[0][0]
            T_index.append(t_index)
            t_val = curr_beat[t_index]
            T_value.append(t_val)
        
        R_index = np.array(R_index).reshape(-1,1)
        R_value = np.array(R_value).reshape(-1,1)
        Q_index = np.array(Q_index).reshape(-1,1)
        Q_value = np.array(Q_value).reshape(-1,1)
        P_index = np.array(P_index).reshape(-1,1)
        P_value = np.array(P_value).reshape(-1,1)
        S_index = np.array(S_index).reshape(-1,1)
        S_value = np.array(S_value).reshape(-1,1)
        T_index = np.array(T_index).reshape(-1,1)
        T_value = np.array(T_value).reshape(-1,1)

        

        peak_data_all = np.concatenate((R_index, R_value, Q_index, Q_value, P_index, P_value, S_index, S_value, T_index, T_value), axis = 1)
        peak_data_vals = np.concatenate((R_value, Q_value, P_value, S_value, T_value), axis = 1)

        return peak_data_all, peak_data_vals
    

    def get_peak_segments_features(self, peakdata_all, peak_data_vals, feature_set):
        '''
        This method returns a matrix containing the peak
        segments PR QRS ST of each patient
        '''
        PR = peakdata_all[:, 0] - peakdata_all[:, 4]
        QRS = peakdata_all[:, 6] - peakdata_all[:, 3]
        ST = peakdata_all[:, 8] - peakdata_all[:, 6]

        data = np.stack((PR, QRS, ST), axis = 1)
        feature_set = np.concatenate((feature_set, data, peak_data_vals), axis = 1)
        return feature_set
    
    # TODO: don't know don't care whether good features
    def extract_time_domain_features_ecg(self, time_domain_data, feature_set):
        ts_list, filtered_list, rpeaks_list, templates_ts_list, templates_list, heart_rate_ts_list, heart_rate_list = self.get_ecg_signals(time_domain_data)
        mean_normalized_heart_beat_amplitudes = self.get_heartbeat(templates_list)
        peaks, peak_data_vals = self.extract_peak_features(mean_normalized_heart_beat_amplitudes)
        
        vprint("Extracting statistical features of mean normalized heartbeats.")
        feature_set = self.extract_heartbeat_statistical_features(mean_normalized_heart_beat_amplitudes, feature_set)
        vprint("Extracting QRSPT-peaks & -segment features of mean normalized heartbeats.")
        feature_set = self.get_peak_segments_features(peaks, peak_data_vals, feature_set=feature_set)
        return feature_set


    def extract_timedomain_features_for_nn_intervals(self, time_domain_data, feature_set):

        """
        Updates feature_set with a range of time domain features for HRV analysis.

        time_domain_features = {
            'mean_nni': mean_nni,
            'sdnn': sdnn,
            'sdsd': sdsd,
            'nni_50': nni_50,
            'pnni_50': pnni_50,
            'nni_20': nni_20,
            'pnni_20': pnni_20,
            'rmssd': rmssd,
            'median_nni': median_nni,
            'range_nni': range_nni,
            'cvsd': cvsd,
            'cvnni': cvnni,
            'mean_hr': mean_hr,
            "max_hr": max_hr,
            "min_hr": min_hr,
            "std_hr": std_hr,
        }
        """
        
        mean_nni_list = []
        sdnn_list = []
        sdsd_list = []
        nni_50_list = []
        pnni_50_list = []
        nni_20_list = []
        pnni_20_list = []
        rmssd_list = []
        median_nni_list = []
        range_nni_list = []
        cvsd_list = []
        cvnni_list = []
        mean_hr_list = []
        max_hr_list = []
        min_hr_list = []
        std_hr_list = []

        for row_vec in tqdm(time_domain_data):
            # drop nans 
            row_vec = row_vec[~np.isnan(row_vec)]
            nn_intervals = self.get_nn_interval_single_patient(row_vec)
            # Some patients have only one nn interval, which is not enough for the hrv analysis -> Division by zero
            if len(nn_intervals) > 1:
                time_domain_features = get_time_domain_features(nn_intervals=nn_intervals)
                mean_nni_list.append(time_domain_features['mean_nni'])
                sdnn_list.append(time_domain_features['sdnn'])
                sdsd_list.append(time_domain_features['sdsd'])
                nni_50_list.append(time_domain_features['nni_50'])
                pnni_50_list.append(time_domain_features['pnni_50'])
                nni_20_list.append(time_domain_features['nni_20'])
                pnni_20_list.append(time_domain_features['pnni_20'])
                rmssd_list.append(time_domain_features['rmssd'])
                median_nni_list.append(time_domain_features['median_nni'])
                range_nni_list.append(time_domain_features['range_nni'])
                cvsd_list.append(time_domain_features['cvsd'])
                cvnni_list.append(time_domain_features['cvnni'])
                mean_hr_list.append(time_domain_features['mean_hr'])
                max_hr_list.append(time_domain_features['max_hr'])
                min_hr_list.append(time_domain_features['min_hr'])
                std_hr_list.append(time_domain_features['std_hr'])
            else:
                mean_nni_list.append(np.nan)
                sdnn_list.append(np.nan)
                sdsd_list.append(np.nan)
                nni_50_list.append(np.nan)
                pnni_50_list.append(np.nan)
                nni_20_list.append(np.nan)
                pnni_20_list.append(np.nan)
                rmssd_list.append(np.nan)
                median_nni_list.append(np.nan)
                range_nni_list.append(np.nan)
                cvsd_list.append(np.nan)
                cvnni_list.append(np.nan)
                mean_hr_list.append(np.nan)
                max_hr_list.append(np.nan)
                min_hr_list.append(np.nan)
                std_hr_list.append(np.nan)

        # cast to np.array and transpose it such that can be concatenated later to one data matrix
        mean_nni_list = np.array(mean_nni_list).reshape(-1,1)
        sdnn_list = np.array(sdnn_list).reshape(-1,1)
        sdsd_list = np.array(sdsd_list).reshape(-1,1)
        nni_50_list = np.array(nni_50_list).reshape(-1,1)
        pnni_50_list = np.array(pnni_50_list).reshape(-1,1)
        nni_20_list = np.array(nni_20_list).reshape(-1,1)
        pnni_20_list = np.array(pnni_20_list).reshape(-1,1)
        rmssd_list = np.array(rmssd_list).reshape(-1,1)
        median_nni_list = np.array(median_nni_list).reshape(-1,1)
        range_nni_list = np.array(range_nni_list).reshape(-1,1)
        cvsd_list = np.array(cvsd_list).reshape(-1,1)
        cvnni_list = np.array(cvnni_list).reshape(-1,1)
        mean_hr_list = np.array(mean_hr_list).reshape(-1,1)
        max_hr_list = np.array(max_hr_list).reshape(-1,1)
        min_hr_list = np.array(min_hr_list).reshape(-1,1)
        std_hr_list = np.array(std_hr_list).reshape(-1,1)

        time_domain_features = np.concatenate((mean_nni_list, sdnn_list, sdsd_list, nni_50_list, pnni_50_list, nni_20_list, pnni_20_list,
                                            rmssd_list, median_nni_list, range_nni_list, cvsd_list, cvnni_list, mean_hr_list, max_hr_list,
                                            min_hr_list, std_hr_list), axis=1)
                                            

        return np.concatenate((feature_set, time_domain_features), axis=1)


    def extract_frequency_domain_features(self, time_domain_data, feature_set):
        # Transform data to frequency domain
        """
        Updates feature_set to store the frequency domain features

        Returns a dictionary containing frequency domain features for HRV analyses:
        - **total_power** : Total power density spectral
        - **vlf** : variance ( = power ) in HRV in the Very low Frequency (.003 to .04 Hz by default). \
        Reflect an intrinsic rhythm produced by the heart which is modulated primarily by sympathetic \
        activity.
        - **lf** : variance ( = power ) in HRV in the low Frequency (.04 to .15 Hz). Reflects a \
        mixture of sympathetic and parasympathetic activity, but in long-term recordings, it reflects \
        sympathetic activity and can be reduced by the beta-adrenergic antagonist propanolol.
        - **hf**: variance ( = power ) in HRV in the High Frequency (.15 to .40 Hz by default). \
        Reflects fast changes in beat-to-beat variability due to parasympathetic (vagal) activity. \
        Sometimes called the respiratory band because it corresponds to HRV changes related to the \
        respiratory cycle and can be increased by slow, deep breathing (about 6 or 7 breaths per \
        minute) and decreased by anticholinergic drugs or vagal blockade.
        - **lf_hf_ratio** : lf/hf ratio is sometimes used by some investigators as a quantitative \
        mirror of the sympatho/vagal balance.
        - **lfnu** : normalized lf power.
        - **hfnu** : normalized hf power.

        """
        total_power_list = []
        vlf_list = []
        lf_list = []
        hf_list = []
        lf_hf_ratio_list = []
        lfnu_list = []
        hfnu_list = []

        for row_vec in tqdm(time_domain_data):
            # remove nan values
            row_vec = row_vec[~np.isnan(row_vec)]
            nn_intervals = self.get_nn_interval_single_patient(row_vec)
            if len(nn_intervals) > 1:
                freq_domain_features = get_frequency_domain_features(nn_intervals=nn_intervals)
                total_power_list.append(freq_domain_features['total_power'])
                vlf_list.append(freq_domain_features['vlf'])
                lf_list.append(freq_domain_features['lf'])
                hf_list.append(freq_domain_features['hf'])
                lf_hf_ratio_list.append(freq_domain_features['lf_hf_ratio'])
                lfnu_list.append(freq_domain_features['lfnu'])
                hfnu_list.append(freq_domain_features['hfnu'])
            else:
                total_power_list.append(np.nan)
                vlf_list.append(np.nan)
                lf_list.append(np.nan)
                hf_list.append(np.nan)
                lf_hf_ratio_list.append(np.nan)
                lfnu_list.append(np.nan)
                hfnu_list.append(np.nan)

        # cast to np.array and transpose it such that can be concatenated later to one data matrix
        total_power_list = np.array(total_power_list).reshape(-1,1)
        vlf_list = np.array(vlf_list).reshape(-1,1)
        lf_list = np.array(total_power_list).reshape(-1,1)
        hf_list = np.array(hf_list).reshape(-1,1)
        lf_hf_ratio_list = np.array(lf_hf_ratio_list).reshape(-1,1)
        lfnu_list = np.array(lfnu_list).reshape(-1,1)
        hfnu_list = np.array(hfnu_list).reshape(-1,1)

        freq_features = np.concatenate((total_power_list, vlf_list, lf_list, hf_list, lf_hf_ratio_list, lfnu_list, hfnu_list), axis=1)

        return np.concatenate((feature_set, freq_features), axis=1)
    

    def extract_wavelet_coefficents(self, time_domain_data, feature_set):
        wavelet_approximations = []
        wavelet_coefficents = []
        for patient in time_domain_data:
            (cA, cD) = pywt.dwt(patient, 'db1')
            wavelet_approximations.append(cA)
            wavelet_coefficents.append(cD)
        wavelet_approximations = np.array(wavelet_approximations).reshape(-1,1)
        wavelet_coefficents = np.array(wavelet_coefficents).reshape(-1,1)
        wavelets = np.concatenate((wavelet_approximations, wavelet_coefficents), axis=1)
        return np.concatenate((feature_set, wavelets), axis=1)

    # we can also try a run with e.g. waveletname = ['rbio3.1', db1, db2, db3] etc.
    # if different waveltname, not necessary 11 coefficents arrays anymore
    def extract_wavelet_features(self, time_domain_data, feature_set, waveletname = 'db4'):
        all_entropy = []
        all_zero_crossings = []
        all_mean_crossings = []
        all_n5 = []
        all_n25 = []
        all_n75 = []
        all_n95 = []
        all_median = []
        all_mean = []
        all_std = []
        all_var = []
        all_rms = []

        # go through patients
        for patient in tqdm(time_domain_data):
            patient_features = []

            # get 12 statistical values per coefficent array (11*12 features per patient)
            # drop nans
            patient = patient[~np.isnan(patient)]

            # get coefficenty of wavelets, all should have dimesion (11,)
            coefficents = pywt.wavedec(patient, waveletname)

            # nan array to append if something weird with lengths happens
            nans = [np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan]

            # coefficents needs to have 11 arrays "coeff" (of different shape)
            for coef in coefficents:
                # if coeff has length zero (for whatever reason), add nan vec for correct dimesionality
                if(len(coef) == 0):
                    # TODO doublecheck, but if empty coeff array than no statistics make sense / will crash
                    patient_features += nans
                else:
                    # TODO: Is this allowed?
                    # At the moment, most patients have 11 coefficents arrays, but some have more, trim them down
                    # if(len(coef) > 11):
                    #     coef = coef[:11]

                    patient_features += self.get_wavelet_features(coef)
            
            # if coefficents doesnt has length 11, append 11-len(coefficents) times the nan array
            if(len(coefficents) < 11):
                for i in range(11-len(coefficents)):
                    patient_features += nans

            # TODO: Is this allowed?
            # At the moment, most patients have 11 coefficents arrays, but some have more, trim them down
            patient_features = patient_features[:132]
            
            # TODO: next line necessary / correct ? Yes      
            # if in any weird case (i dont know why) there would be more than 11 coefficent arrays, cut off at 132th element
            # patient_features = patient_features[0:132]
            # append in this manner: [[11 entropies of patient x], [11 entropies of patient x+1], ...[]] 
            all_entropy.append(np.array(patient_features[0::12]))
            all_zero_crossings.append(np.array(patient_features[1::12]))
            all_mean_crossings.append(np.array(patient_features[2::12]))
            all_n5.append(np.array(patient_features[3::12]))
            all_n25.append(np.array(patient_features[4::12]))
            all_n75.append(np.array(patient_features[5::12]))
            all_n95.append(np.array(patient_features[6::12]))
            all_median.append(np.array(patient_features[7::12]))
            all_mean.append(np.array(patient_features[8::12]))
            all_std.append(np.array(patient_features[9::12]))
            all_var.append(np.array(patient_features[10::12]))
            all_rms.append(np.array(patient_features[11::12]))


        # TODO: Double check dimensions!!!
        feature_set = np.concatenate((feature_set, all_entropy, all_zero_crossings, all_mean_crossings, all_n5, all_n25,
                                    all_n75, all_n95, all_median, all_mean, all_std, all_var, all_rms), axis = 1)

        return feature_set


    def calc_entropy_wavelets(self, values):
        counter_vals = Counter(values).most_common()
        # TODO: Check whether makes sense, i think so
        if(len(values) == 0):
            return np.nan
        probs = [elem[1]/len(values) for elem in counter_vals]
        entropy = scipy.stats.entropy(probs)
        return entropy
    
    def calc_statistics_wavelets(self, values):
        n5 = np.nanpercentile(values, 5)
        n25 = np.nanpercentile(values, 25)
        n75 = np.nanpercentile(values, 75)
        n95 = np.nanpercentile(values, 95)
        median = np.nanpercentile(values, 50)
        mean = np.nanmean(values)
        std = np.nanstd(values)
        var = np.nanvar(values)
        rms = np.nanmean(np.sqrt(values**2))
        return [n5, n25, n75, n95, median, mean, std, var, rms]

    
    def calculate_crossings_wavelets(self, values):
        zero_crossing_indices = np.nonzero(np.diff(np.array(values) > 0))[0]
        no_zero_crossings = len(zero_crossing_indices)
        mean_crossing_indices = np.nonzero(np.diff(np.array(values) > np.nanmean(values)))[0]
        no_mean_crossings = len(mean_crossing_indices)
        return [no_zero_crossings, no_mean_crossings]
    
    def get_wavelet_features(self, values):
        entropy = self.calc_entropy_wavelets(values)
        crossings = self.calculate_crossings_wavelets(values)
        statistics = self.calc_statistics_wavelets(values)
        return [entropy] + crossings + statistics



    def feature_extraction(self, time_domain_data, feature_set, postfix=""):
        """
        collects all the features and stores into one matrix
        """
        vprint("Extracting features.")
        # dprint(feature_set)
        # feature_set = self.extract_statistical_features(time_domain_data, feature_set)
        # np.savetxt(f"train_feature_set_statistics_{postfix}.csv", feature_set, delimiter=",")
        # dprint(feature_set)
        # feature_set = self.extract_timedomain_features_for_nn_intervals(time_domain_data, feature_set)
        # np.savetxt(f"train_feature_set_stat_timenn_{postfix}.csv", feature_set, delimiter=",")
        # dprint(feature_set)
        # feature_set = self.extract_frequency_domain_features(time_domain_data, feature_set)
        # np.savetxt(f"train_feature_set_stat_timenn_freq_{postfix}.csv", feature_set, delimiter=",")
        # dprint(feature_set)
        # feature_set = self.extract_wavelet_features(time_domain_data, feature_set)
        # np.savetxt(f"train_feature_set_stat_timenn_freq_wave_{postfix}.csv", feature_set, delimiter=",")
        # dprint(feature_set)
        # feature_set = self.extract_time_domain_features(time_domain_data, feature_set)
        # np.savetxt(f"train_feature_set_stat_timenn_freq_wave_timedom_{postfix}.csv", feature_set, delimiter=",")
        # dprint(feature_set)
        feature_set = self.extract_time_domain_features_ecg(time_domain_data, feature_set)
        # np.savetxt(f"train_feature_set_stat_timenn_freq_wave_timedomecg_{postfix}.csv", feature_set, delimiter=",")
        dprint(feature_set)

        return feature_set


    # Helper functions ----------------------------------------------------------------------------------------
    def get_nn_interval_single_patient(self, patient):
        # get r_peak indices
        rpeak_indices = ecg.christov_segmenter(signal=patient,sampling_rate=300)[0]
        if len(rpeak_indices) > 1:
            
            # get outliers replaced by nans
            rr_intervals_without_outliers = self.replace_outlier_rr_intervals_by_nan(rpeak_indices)
            # interpolate outliers
            interpolated_rr_intervals = interpolate_nan_values(rr_intervals=rr_intervals_without_outliers, 
                                                    interpolation_method="linear")
            # replace etopic beats by nans                                           
            nn_intervals_list = remove_ectopic_beats(rr_intervals=interpolated_rr_intervals, method="malik")
            # interpolate nans
            interpolated_nn_intervals = np.asarray(interpolate_nan_values(rr_intervals=nn_intervals_list))
            # drop nans at edges (not interpolated nans)
            interpolated_nn_intervals = interpolated_nn_intervals[~(np.isnan(interpolated_nn_intervals))]
            return interpolated_nn_intervals
        return np.asarray([])
    
    def replace_outlier_rr_intervals_by_nan(self, rpeak_indices):
        # get rr intervals
        rpeak_intervals = np.diff(rpeak_indices)
        # get upper lower bounds
        mean_rpeak_interval = np.nanmean(rpeak_intervals)
        std_rpeak_interval = np.nanstd(rpeak_intervals)
        upperbound = mean_rpeak_interval + 2*std_rpeak_interval
        lowerbound = mean_rpeak_interval - 2*std_rpeak_interval
        # remove outliers
        rr_intervals_without_outliers = remove_outliers(rr_intervals=rpeak_intervals, verbose=False,  
                                                low_rri=lowerbound, high_rri=upperbound)
        return rr_intervals_without_outliers


    def drop_r_indices_of_outlier_intervals(self, rpeak_indices):
        # get rr intervals
        rpeak_intervals = np.diff(rpeak_indices)
        # get upper lower bounds
        mean_rpeak_interval = np.nanmean(rpeak_intervals)
        std_rpeak_interval = np.nanstd(rpeak_intervals)
        upperbound = mean_rpeak_interval + 2*std_rpeak_interval
        lowerbound = mean_rpeak_interval - 2*std_rpeak_interval
        # remove outliers
        rr_intervals_without_outliers = remove_outliers(rr_intervals=rpeak_intervals, verbose=False,  
                                                low_rri=lowerbound, high_rri=upperbound)
        # get nan indices                                        
        nan_indices = np.argwhere(np.isnan(rr_intervals_without_outliers))

        # np array
        rr_intervals_without_outliers = np.array(rr_intervals_without_outliers)
        # remove all r peaks where oultlier interval was detected,
        # e.g. outlier interval at position i, remove rpeaks i and i+1
        for i in range(len(nan_indices)-1):
            rpeak_indices[i:i+1] = np.nan
        rpeak_indices = np.argwhere(np.isnan(rpeak_indices))
        return rpeak_indices


def vprint(message):
    if VERBOSE:
        print(message)

def dprint(message):
    if DEBUG:
        print(message)

def load_data():
    '''
    Load the training and test data.
    '''
    vprint('Loading datasets')
    train_features = pd.read_csv('X_train.csv', index_col='id').to_numpy()
    train_GT = pd.read_csv('y_train.csv', index_col='id').to_numpy()
    test_features= pd.read_csv('X_test.csv', index_col='id').to_numpy()
    return train_features, train_GT, test_features

def save_data(data, filename):
    vprint('Saving predictions to csv file')
    idx = range(len(data))
    pd.Series(data, idx).to_csv(filename, index_label='id', header=['y'])


def main():

    # Initialize the model
    model = ECGClassificationModel()

    if LOAD_FEATURES:
        vprint("Loading features.")
        # TODO: Change which features to load
        train_feature_set = np.loadtxt("train_feature_set_continued.csv", delimiter=",")
        test_feature_set = np.loadtxt("test_feature_set_continued.csv", delimiter=",")
        train_GT = pd.read_csv('y_train.csv', index_col='id').to_numpy()
        vprint(f"Train shape: {np.shape(train_feature_set)}, Test shape: {np.shape(test_feature_set)}, GT shape: {np.shape(train_GT)}")

        if CONTINUE_LOADING_FEATURES:
            vprint("Loading more features.")
            # Load the training dateset and test features
            train_features, train_GT, test_features = load_data()
            
            # TODO: Uncomment if extraction with baseline removal
            # vprint("Extracting features with baseline removal.")
            # for j in range(0, len(train_features[:,0])):
            #     row = train_features[j,:]
            #     # Remove NaNs to determine the baseline of the signal
            #     baseline_set = row[~np.isnan(row)]
            #     baseline_less = heartpy.remove_baseline_wander(baseline_set, 300, cutoff=0.05)
            #     train_features[j, :len(baseline_less)] = baseline_less

            train_feature_set = model.feature_extraction(train_features, train_feature_set)
            np.savetxt(f"train_feature_set_continued.csv", train_feature_set, delimiter=",")

            # TODO: Uncomment if extraction with baseline removal
            # for j in range(0, len(test_features[:,0])):
            #     row = test_features[j,:]
            #     baseline_set = row[~np.isnan(row)]
            #     baseline_less = heartpy.remove_baseline_wander(baseline_set, 300, cutoff=0.05)
            #     test_features[j, :len(baseline_less)] = baseline_less

            test_feature_set = model.feature_extraction(test_features, test_feature_set)
            np.savetxt(f"test_feature_set_continued.csv", test_feature_set, delimiter=",")


    else:
        # Load the training dateset and test features
        train_features, train_GT, test_features = load_data()

        for i in ["without_baseline_removal", "with_baseline_removal"]:
            # TODO: Remove the baseline from the data?

            # TODO: Remove outlier R-R intervals where we most likely have segmentation errors.-- add later 

            # TODO: Extract features from the data
            train_feature_set = np.empty((train_features.shape[0], 0))
            if i == "with_baseline_removal":
                vprint("Extracting features with baseline removal.")
                for j in range(0, len(train_features[:,0])):
                    row = train_features[j,:]
                    # Remove NaNs to determine the baseline of the signal
                    baseline_set = row[~np.isnan(row)]
                    baseline_less = heartpy.remove_baseline_wander(baseline_set, 300, cutoff=0.05)
                    train_features[j, :len(baseline_less)] = baseline_less
            train_feature_set = model.feature_extraction(train_features, train_feature_set, i)
            np.savetxt(f"train_feature_set_{i}.csv", train_feature_set, delimiter=",")
            test_feature_set = np.empty((test_features.shape[0], 0))
            if i == "with_baseline_removal":
                for j in range(0, len(test_features[:,0])):
                    row = test_features[j,:]
                    baseline_set = row[~np.isnan(row)]
                    baseline_less = heartpy.remove_baseline_wander(baseline_set, 300, cutoff=0.05)
                    test_features[j, :len(baseline_less)] = baseline_less
            test_feature_set = model.feature_extraction(test_features, test_feature_set, i)
            np.savetxt(f"test_feature_set_{i}.csv", test_feature_set, delimiter=",")

    # impute nans
    vprint("Imputing feature sets.")
    # TODO: Possibly better with KNN imputer?
    imp = SimpleImputer(missing_values=np.nan, strategy='median')
    imp.fit(train_feature_set)
    train_feature_set = imp.transform(train_feature_set)

    imp = SimpleImputer(missing_values=np.nan, strategy='median')
    imp.fit(test_feature_set)
    test_feature_set = imp.transform(test_feature_set)

    vprint("Normalize the features.")
    # TODO: Normalize the data? In time and also amplitude?
    scaler = StandardScaler()
    scaler.fit(train_feature_set)
    train_feature_set = scaler.transform(train_feature_set)
    test_feature_set = scaler.transform(test_feature_set)

    # We currently have 158 features
    vprint(f"Select the {model.k_features} best features.")
    # TODO: Select the best features if we have too many, or in any case?
    selector = SelectKBest(mutual_info_classif, k=model.k_features)
    selector.fit(train_feature_set, train_GT.ravel())
    train_feature_set = selector.transform(train_feature_set)
    test_feature_set = selector.transform(test_feature_set)

    # Split the training dataset into training and validation sets for repeated k-fold cross validation
    if GRIDSEARCH:
        vprint('Splitting the training dataset for repeated k-fold cross validation & random search.')
        cross_val = RepeatedKFold(n_splits=model.k_splits, n_repeats=model.k_repeats, random_state=model.seed)
        # Train the model using repeated k-fold cross validation and randomized search
        # TODO: Check that the class weights are properly taken into account
        search = RandomizedSearchCV(model.model, model.get_param_grid(), n_iter=model.num_param_samples, cv=cross_val, scoring='f1_micro', n_jobs=1, verbose=3 if VERBOSE else 1, random_state=model.seed)
        results = search.fit(train_feature_set, train_GT.ravel())
        model.model = results.best_estimator_

    vprint("Predicting the test data.")
    # TODO: Train the best model on all the data now and predict on the test set
    model.fit_model(train_feature_set, train_GT)
    predictions = model.predict(test_feature_set)
    
    vprint("Saving the predictions.")
    # TODO: Save the predictions to a csv file
    save_data(predictions, 'pred_test.csv')


if __name__ == "__main__":
    main()

