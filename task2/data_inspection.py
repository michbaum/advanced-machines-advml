#%%
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import umap
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN

#%%

def main():
    # Load the data
    train_features = pd.read_csv('X_train.csv')
    test_features = pd.read_csv('X_test.csv')
    train_GT_pre_imp = pd.read_csv('y_train.csv')

    # Visualize the data
    # print(train_features.info(max_cols=850))
    # print(test_features.info(max_cols=850))
    # print(train_GT_pre_imp.info())

    # Most columns don't have that many missing valuse, never more than 10%

    # print(train_features.describe())
    # print(test_features.describe())
    # print(train_GT_pre_imp.describe())

    # Saerch the features with the most NaNs
    print(train_features.isnull().sum().sort_values(ascending=False)[0:20])

    # Search the features with the most zeros
    print(train_features.isin([0]).sum().sort_values(ascending=False)[0:10])

    # Check the distribution of the target variable with lots of zeros
    # print(train_features['x143'].describe())
    # It sucks, delete it

    train_features = train_features.to_numpy()[:, 1:]


if __name__ == "__main__":
    main()
