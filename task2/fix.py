# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.signal

from neurokit2.epochs import epochs_create, epochs_to_df
from neurokit2.signal import (signal_findpeaks, signal_formatpeaks, signal_rate,
                      signal_resample, signal_smooth, signal_zerocrossings)
from neurokit2.ecg.ecg_delineate import all
from neurokit2.stats import standardize
from neurokit2.ecg.ecg_peaks import ecg_peaks
from neurokit2.ecg.ecg_segment import ecg_segment


def _dwt_resample_points(peaks, sampling_rate, desired_sampling_rate):
    """Resample given points to a different sampling rate."""
    if isinstance(peaks, np.ndarray):  # peaks are passed in from previous processing steps
        # Prevent overflow by converting to np.int64 (peaks might be passed in containing np.int32).
        peaks = peaks.astype(dtype=np.int64)
    elif isinstance(peaks, list):  # peaks returned from internal functions
        # Cannot be converted to int since list might contain np.nan. Automatically cast to np.float64 if list contains np.nan.
        peaks = np.array(peaks)
    peaks_resample = peaks * desired_sampling_rate / sampling_rate
    peaks_resample = [np.nan if np.isnan(x) else int(x) for x in peaks_resample.tolist()]
    return peaks_resample


def _dwt_ecg_del(ecg, rpeaks, sampling_rate, analysis_sampling_rate=2000):
    """Delinate ecg signal using discrete wavelet transforms.

    Parameters
    ----------
    ecg : Union[list, np.array, pd.Series]
        The cleaned ECG channel as returned by `ecg_clean()`.
    rpeaks : Union[list, np.array, pd.Series]
        The samples at which R-peaks occur. Accessible with the key "ECG_R_Peaks" in the info dictionary
        returned by `ecg_findpeaks()`.
    sampling_rate : int
        The sampling frequency of `ecg_signal` (in Hz, i.e., samples/second).
    analysis_sampling_rate : int
        The sampling frequency for analysis (in Hz, i.e., samples/second).

    Returns
    --------
    dict
        Dictionary of the points.

    """
    # No dwt defined method for Q and S peak
    # Adopting manual method from "peak" method
    qpeaks = []
    speaks = []

    heartbeats = ecg_segment(ecg, rpeaks, sampling_rate=sampling_rate)
    for i, rpeak in enumerate(rpeaks):
        heartbeat = heartbeats[str(i + 1)]
        # Get index of R peaks
        R = heartbeat.index.get_loc(np.min(heartbeat.index.values[heartbeat.index.values > 0]))
        # Q wave
        Q_index, Q = _ecg_delineator_peak_Q(rpeak, heartbeat, R)
        qpeaks.append(Q_index)
        # S wave
        S_index, S = _ecg_delineator_peak_S(rpeak, heartbeat)
        speaks.append(S_index)

    # dwt to delineate tp waves, onsets, offsets and qrs ontsets and offsets
    ecg = signal_resample(
        ecg, sampling_rate=sampling_rate, desired_sampling_rate=analysis_sampling_rate
    )
    dwtmatr = _dwt_compute_multiscales(ecg, 9)

    # # only for debugging
    # for idx in [0, 1, 2, 3]:
    #     plt.plot(dwtmatr[idx + 3], label=f'W[{idx}]')
    # plt.plot(ecg, '--')
    # plt.legend()
    # plt.grid(True)
    # plt.show()
    rpeaks_resampled = _dwt_resample_points(rpeaks, sampling_rate, analysis_sampling_rate)

    tpeaks, ppeaks = _dwt_delineate_tp_peaks(
        ecg, rpeaks_resampled, dwtmatr, sampling_rate=analysis_sampling_rate
    )
    qrs_onsets, qrs_offsets = _dwt_delineate_qrs_bounds(
        rpeaks_resampled, dwtmatr, ppeaks, tpeaks, sampling_rate=analysis_sampling_rate
    )
    ponsets, poffsets = _dwt_delineate_tp_onsets_offsets(
        ppeaks, rpeaks_resampled, dwtmatr, sampling_rate=analysis_sampling_rate
    )
    tonsets, toffsets = _dwt_delineate_tp_onsets_offsets(
        tpeaks,
        rpeaks_resampled,
        dwtmatr,
        sampling_rate=analysis_sampling_rate,
        onset_weight=0.6,
        duration_onset=0.6,
    )

    return dict(
        ECG_P_Peaks=_dwt_resample_points(
            ppeaks, analysis_sampling_rate, desired_sampling_rate=sampling_rate
        ),
        ECG_P_Onsets=_dwt_resample_points(
            ponsets, analysis_sampling_rate, desired_sampling_rate=sampling_rate
        ),
        ECG_P_Offsets=_dwt_resample_points(
            poffsets, analysis_sampling_rate, desired_sampling_rate=sampling_rate
        ),
        ECG_Q_Peaks=qpeaks,
        ECG_R_Onsets=_dwt_resample_points(
            qrs_onsets, analysis_sampling_rate, desired_sampling_rate=sampling_rate
        ),
        ECG_R_Offsets=_dwt_resample_points(
            qrs_offsets, analysis_sampling_rate, desired_sampling_rate=sampling_rate
        ),
        ECG_S_Peaks=speaks,
        ECG_T_Peaks=_dwt_resample_points(
            tpeaks, analysis_sampling_rate, desired_sampling_rate=sampling_rate
        ),
        ECG_T_Onsets=_dwt_resample_points(
            tonsets, analysis_sampling_rate, desired_sampling_rate=sampling_rate
        ),
        ECG_T_Offsets=_dwt_resample_points(
            toffsets, analysis_sampling_rate, desired_sampling_rate=sampling_rate
        ),
    )

def ecg_del(
    ecg_cleaned,
    rpeaks=None,
    sampling_rate=1000,
    method="dwt",
    show=False,
    show_type="peaks",
    check=False,
):
    """Delineate QRS complex.

    Function to delineate the QRS complex.

    - **Cardiac Cycle**: A typical ECG heartbeat consists of a P wave, a QRS complex and a T wave.
      The P wave represents the wave of depolarization that spreads from the SA-node throughout the atria.
      The QRS complex reflects the rapid depolarization of the right and left ventricles. Since the
      ventricles are the largest part of the heart, in terms of mass, the QRS complex usually has a much
      larger amplitude than the P-wave. The T wave represents the ventricular repolarization of the
      ventricles.On rare occasions, a U wave can be seen following the T wave. The U wave is believed
      to be related to the last remnants of ventricular repolarization.

    Parameters
    ----------
    ecg_cleaned : Union[list, np.array, pd.Series]
        The cleaned ECG channel as returned by `ecg_clean()`.
    rpeaks : Union[list, np.array, pd.Series]
        The samples at which R-peaks occur. Accessible with the key "ECG_R_Peaks" in the info dictionary
        returned by `ecg_findpeaks()`.
    sampling_rate : int
        The sampling frequency of `ecg_signal` (in Hz, i.e., samples/second).
        Defaults to 500.
    method : str
        Can be one of 'peak' for a peak-based method, 'cwt' for continuous wavelet transform
        or 'dwt' (default) for discrete wavelet transform.
    show : bool
        If True, will return a plot to visualizing the delineated waves
        information.
    show_type: str
        The type of delineated waves information showed in the plot.
        Can be "peaks", "bounds_R", "bounds_T", "bounds_P" or "all".
    check : bool
        Defaults to False. If True, replaces the delineated features with np.nan if its standardized distance
        from R-peaks is more than 3.

    Returns
    -------
    waves : dict
        A dictionary containing additional information.
        For derivative method, the dictionary contains the samples at which P-peaks, Q-peaks, S-peaks,
        T-peaks, P-onsets and T-offsets occur, accessible with the key "ECG_P_Peaks", "ECG_Q_Peaks",
        "ECG_S_Peaks", "ECG_T_Peaks", "ECG_P_Onsets", "ECG_T_Offsets" respectively.

        For wavelet methods, in addition to the above information, the dictionary contains the samples at which QRS-onsets and
        QRS-offsets occur, accessible with the key "ECG_P_Peaks", "ECG_T_Peaks", "ECG_P_Onsets", "ECG_P_Offsets",
        "ECG_Q_Peaks", "ECG_S_Peaks", "ECG_T_Onsets", "ECG_T_Offsets", "ECG_R_Onsets", "ECG_R_Offsets" respectively.

    signals : DataFrame
        A DataFrame of same length as the input signal in which occurences of
        peaks, onsets and offsets marked as "1" in a list of zeros.

    See Also
    --------
    ecg_clean, signal_fixpeaks, ecg_peaks, signal_rate, ecg_process, ecg_plot

    Examples
    --------
    >>> import neurokit2 as nk
    >>>
    >>> ecg = nk.ecg_simulate(duration=10, sampling_rate=1000)
    >>> cleaned = nk.ecg_clean(ecg, sampling_rate=1000)
    >>> _, rpeaks = nk.ecg_peaks(cleaned, sampling_rate=1000)
    >>> signals, waves = nk.ecg_delineate(cleaned, rpeaks, sampling_rate=1000)
    >>> nk.events_plot(waves["ECG_P_Peaks"], cleaned) #doctest: +ELLIPSIS
    <Figure ...>
    >>> nk.events_plot(waves["ECG_T_Peaks"], cleaned) #doctest: +ELLIPSIS
    <Figure ...>

    References
    --------------
    - Martínez, J. P., Almeida, R., Olmos, S., Rocha, A. P., & Laguna, P. (2004). A wavelet-based ECG
      delineator: evaluation on standard databases. IEEE Transactions on biomedical engineering,
      51(4), 570-581.

    """
    # Sanitize input for ecg_cleaned
    if isinstance(ecg_cleaned, pd.DataFrame):
        cols = [col for col in ecg_cleaned.columns if "ECG_Clean" in col]
        if cols:
            ecg_cleaned = ecg_cleaned[cols[0]].values
        else:
            raise ValueError(
                "NeuroKit error: ecg_delineate(): Wrong input, we couldn't extract"
                "cleaned signal."
            )

    elif isinstance(ecg_cleaned, dict):
        for i in ecg_cleaned:
            cols = [col for col in ecg_cleaned[i].columns if "ECG_Clean" in col]
            if cols:
                signals = epochs_to_df(ecg_cleaned)
                ecg_cleaned = signals[cols[0]].values

            else:
                raise ValueError(
                    "NeuroKit error: ecg_delineate(): Wrong input, we couldn't extract"
                    "cleaned signal."
                )

    # Sanitize input for rpeaks
    if rpeaks is None:
        _, rpeaks = ecg_peaks(ecg_cleaned, sampling_rate=sampling_rate)
        rpeaks = rpeaks["ECG_R_Peaks"]

    if isinstance(rpeaks, dict):
        rpeaks = rpeaks["ECG_R_Peaks"]

    method = method.lower()  # remove capitalised letters
    if method in ["dwt", "discrete wavelet transform"]:
        waves = _dwt_ecg_del(ecg_cleaned, rpeaks, sampling_rate=sampling_rate)

    else:
        raise ValueError(
            "NeuroKit error: ecg_delineate(): 'method' should be one of 'peak'," "'cwt' or 'dwt'."
        )

    # Remove NaN in Peaks, Onsets, and Offsets
    waves_noNA = waves.copy()
    for feature in waves_noNA.keys():
        waves_noNA[feature] = [int(x) for x in waves_noNA[feature] if ~np.isnan(x) and x > 0]

    instant_peaks = signal_formatpeaks(waves_noNA, desired_length=len(ecg_cleaned))
    signals = instant_peaks

    waves_sanitized = {}
    for feature, values in waves.items():
        waves_sanitized[feature] = [x for x in values if x > 0 or x is np.nan]

    if show is True:
        _ecg_delineate_plot(
            ecg_cleaned,
            rpeaks=rpeaks,
            signals=signals,
            signal_features_type=show_type,
            sampling_rate=sampling_rate,
        )

    if check is True:
        waves_sanitized = _ecg_delineate_check(waves_sanitized, rpeaks)

    return signals, waves_sanitized


