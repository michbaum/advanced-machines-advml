Experiment Setup:
Generelles Setup ist ein guter middle ground aller dinge

Tests: (use XGB with 100 n_estimators & max_depth = 4)
- Imputer & Pearson_Threshold (vary the KNN N number)
-- median is close to mean, most_frequent always leads to 0 pearson dropouts :/ and behaves worse
-- varying the KNN N Number: 1214 is the best -> actually mean?
- Feature_Selector & Num_Features
-- selectkbest with features between 25 and 45

- Outlier Detector & GMM_Components & Outlier_estimate
-- 1 - 3 components with 0.05 - 0.13 outlier
- GMM & Outlier_training_set (Train, test, train + labels, train + test)
-- Training on test & train_label seems to lead to the best outlier rejection results (which makes sense)

- Models

- Medium sized grid search over all the best params


TODO:

Maybe impute differently per feature?!
Reduce features even more (maybe best 5, 10 etc.)
Plot GMM densities per data point to see sharp declines
Add SVM to models


Moritz:
- RFE implementieren-------- Für andere Modelle merken.
- Duplicate features with peason correlation --
- maskuliner werden
- geburtstagsgeschenk für arvid 
- Investigate why GMM with >1 components lead to positive log data probability of the data --
- Look into PearsonRConstantInputWarning displayed--- indeces [104, 129, 489, 530] are 0 vectors. 
- test doing feature selection before outlier detection
- GaussianProcessRegressor
- GMM nur auf test trainieren, auf bedies trainieren-- Probas angucken ob große Sprünge. 
- GMM mit Label trainieren ?
- GMM in Gridsearch


Arvid:
- Moritz zur Party einladen
- Most Frequent imputer debuggen
- LO outlier detector debuggen
- regression for data imputation vielleicht anschauen und implementieren


Michael:
- visualisation of data


MODELS = [XGBoost, GaussianProcessRegressor]
IMPUTER_FIXED = True
IMPUTER = 'mean'
FEATURES_FIXED = True
FEATURE_SELECTOR = selectkbest


main:
if IMPUTER_FIXED:
    imputer = IMPUTER
else:
    imputer = ['mean', 'min', 'max', 'most_frequent']
for i in [imputer]:
    if FEATURES_FIXED:
        feature_selector = selectkbest
    else:
        feature_selector = [...]
    for j in [feature_selector]:
        
        if:
        else:
        for k in [outlier_detector]:
            data = outlier_detector(data)
            for l in [models]:
                fit, predict, print best accuracy
                make graphic or csv or something 

For the test data, we also need to normalize it and
use the features that we extracted on the training data