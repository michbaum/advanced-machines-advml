import os
import typing
import numpy as np
import xgboost as xgb
import pandas as pd
import csv
from sklearn.impute import SimpleImputer
from sklearn.metrics import r2_score
from sklearn.decomposition import PCA
from sklearn.mixture import GaussianMixture
from sklearn.model_selection import GridSearchCV, RepeatedKFold, train_test_split
from sklearn.feature_selection import SelectKBest, mutual_info_regression, f_regression
from sklearn.feature_selection import RFE
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import TheilSenRegressor, HuberRegressor, RANSACRegressor, SGDRegressor, Lasso, BayesianRidge, \
    Ridge
from scipy.stats import pearsonr
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.impute import KNNImputer
from sklearn_rvm import EMRVR
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF
from sklearn.svm import SVR
from sklearn.cluster import DBSCAN
import umap

# Global variables
IMPUTER_FIXED = True
FEATURE_FIXED = True
NUM_FEATURES_FIXED = True
OUTLIER_FIXED = True
GMM_COMPONENTS_FIXED = True
USE_PEARSON = False  # Use correlation or not, if false, use pearson fixed true
PEARSON_FIXED = True
OUTLIER_ESTIMATE_FIXED = True
OUTLIER_TRAINING_SET_FIXED = True
MODEL_FIXED = True

# Outlier Rejection with UMAP
# UMAP_COMP = 2 # Dimension of UMAP, default is 2 (2 - 100)
# UMAP_NEIGHBOURS = 2 # Number of neighbors considered for the manifold approximation, default is 15 (1 - 100)

# Fixed parameters
IMPUTER = ['KNN']
FEATURE_SELECTOR = ['selectkbest']
NUM_FEATURES = 200
NUM_FEATURES_AFTER_IMP = 200
OUTLIER_DETECTOR = ['DBS']
PEARSON_THRESHOLD = 0.93
GMM_COMPONENTS = 3
OUTLIER_ESTIMATE = 0.1
OUTLIER_TRAINING_SET = ['train']
MODEL = ['XGB']

# GridSearch Parameters
# IMPUTERS = ['mean', 'KNN', 'median', 'most_frequent'] TODO: Most frequent fails currently due to the outlier detection
IMPUTERS = ['mean', 'KNN'] # 'II' & 'MI' do not converge apparently
KNN_IMPUTER_N = 5 # 50 -> 0.635 35 -> 0.636
FEATURE_SELETCTORS = ['selectkbest', 'PCA']
NUM_FEATURES_START = 150
NUM_FEATURES_END = 251
NUM_FEATURES_STEP = 50
# OUTLIER_DETECTORS = ['GM', 'IF', 'LO']
OUTLIER_DETECTORS = ['GM', 'IF']  # 'IF' takes very long
PEARSON_THRESHOLD_START = 0.91
PEARSON_THRESHOLD_END = 0.98
PEARSON_THRESHOLD_STEP = 0.02
GMM_COMPONENTS_START = 2
GMM_COMPONENTS_END = 4
GMM_COMPONENTS_STEP = 1
OUTLIER_ESTIMATE_START = 0.05
OUTLIER_ESTIMATE_END = 0.16
OUTLIER_ESTIMATE_STEP = 0.01
OUTLIER_TRAINING_SETS = ['train', 'test', 'train_label']  # 'train' and 'train_test' seems worse
MODELS = ['XGB', 'Huber', 'RANSAC', 'SGD', 'Lasso', 'BayesianRidge', 'Ridge', 'RVM',
          'SVR']  # TODO: Add 'RVM', 'TheilSen' too slow


class RegressionModel(object):
    '''
    Gradient Boosted Decision Tree model for regression.
    '''

    def __init__(self, modelname="XGB") -> None:
        '''
        Initialize the model.
        '''
        # Seed RNG for reproducibility
        self.rng = np.random.default_rng(seed=42)
        self.seed = 42

        self.modelname = modelname

        # Instantiation
        if self.modelname == "XGB":
            self.model = xgb.XGBRegressor(
                n_estimators=4000,
                max_depth=4,
                learning_rate=0.03,
                random_state=self.seed,
                n_jobs=-1,
            )
        elif self.modelname == "TheilSen":
            self.model = TheilSenRegressor(
                random_state=self.seed,
                n_jobs=-1
            )
        elif self.modelname == "Huber":
            self.model = HuberRegressor(
                epsilon=1.35,
                # This regulates how many of the samples are considered outliers, must be > 1.0, the smaller the more robust to outliers
            )
        elif self.modelname == "RANSAC":
            self.model = RANSACRegressor(  # Implements LinearRegressor() estimator under the hood
                random_state=self.seed
            )
        elif self.modelname == "SGD":
            self.model = SGDRegressor(
                random_state=self.seed,
                loss='squared_loss',
            )
        elif self.modelname == "Lasso":
            self.model = Lasso(
                random_state=self.seed,
                selection='cyclic'
            )
        elif self.modelname == "BayesianRidge":
            self.model = BayesianRidge(
                tol=1e-3
            )
        elif self.modelname == "Ridge":
            self.model = Ridge(
                random_state=self.seed,
                alpha=1.0
            )
        elif self.modelname == "RVM":
            self.model = EMRVR(
                kernel="rbf",
                gamma='auto')

        if self.modelname == 'GP':
            self.model = GaussianProcessRegressor(RBF(),
                                                  normalize_y=True)  # Note that the kernel's hyperparameters are optimized during fitting.

        if self.modelname == 'Lasso':
            self.model = Lasso(fit_intercept=True, normalize=False, random_state=self.seed)

        if self.modelname == 'SVR':
            self.model = SVR(kernel='rbf', C=1.0, epsilon=0.1)

        # Tuning parameters

        # For model fitting
        self.k_splits = 5
        self.k_repeats = 3

        # For outlier detection
        self.gmm_components = 3
        self.est_outlier = 0.03

        # According to the tutorial around 200 features were used.
        # For Feature Selection
        # Heuristics point to anywhere between 50 and 150 features
        self.num_features = 150
        self.selectkbest = SelectKBest(f_regression, k=self.num_features)
        self.n_components_pca = self.num_features
        self.pca = PCA(n_components=self.n_components_pca)
        self.pearson_threshold = 0.90  # TODO Number of redundant features is VERY dependent on this threshold. ~9 for 0.95 and ~230 for 0.90

    def get_param_grid(self):
        # TODO: All of these are hyperparameters that should be tuned
        if self.modelname == "XGB":
            param_grid = {
                'n_estimators': [4000],  # range(140, 201, 5),
                'max_depth': [4],  # range(4, 6, 1),
                'learning_rate': [0.03],
                'random_state': [self.seed],
                'n_jobs': [-1]
            }
        elif self.modelname == "TheilSen":
            param_grid = {
                'random_state': [self.seed],
                'n_jobs': [-1]
            }
        elif self.modelname == "Huber":
            param_grid = {
                'epsilon': [1.1, 1.2, 1.3, 1.35, 1.4, 1.5, 2, 5, 10]
            }
        elif self.modelname == "RANSAC":
            param_grid = {
                'random_state': [self.seed]
            }
        elif self.modelname == "SGD":
            param_grid = {
                'random_state': [self.seed],
                'loss': ['squared_loss', 'huber']  # Huber is more robust to outliers
            }
        elif self.modelname == "Lasso":
            param_grid = {
                'random_state': [self.seed],
                'selection': ['cyclic', 'random']
            }
        elif self.modelname == "BayesianRidge":
            param_grid = {
                'tol': [1e-2, 1e-3, 1e-4, 1e-5, 1e-6]
            }
        elif self.modelname == "Ridge":
            param_grid = {
                'random_state': [self.seed],
                'alpha': [0.01, 0.1, 1.0, 10, 100, 1000, 10000]  # Regularization strength
            }
        elif self.modelname == "RVM":
            param_grid = {
                'gamma': ['auto', 'scale'],
                'threshold_alpha': [10, 5000, 100000]
            }
        elif self.modelname == 'GP':
            param_grid = {
            }
        elif self.modelname == 'SVR':
            param_grid = {
                'kernel': ['rbf', 'linear'],
                'C': [0.01, 0.1, 1, 10],
                'epsilon': [0.01, 0.1, 1]
            }

        # TODO: Add logic for more models
        return param_grid

    def fit_model(self, train_features, train_GT) -> None:
        '''
        Fit the model to the data
        '''
        self.model.fit(train_features, train_GT)

    def predict(self, test_features) -> np.ndarray:
        '''
        Predict the age of the patient based on the MRI features
        '''
        return self.model.predict(test_features)

    def accuracy(self, y_pred, y_true):
        return r2_score(y_true, y_pred)

    def impute(self, X_train, strategy) -> np.ndarray:
        """
        strategies: "mean, "median", "most_frequent", "constant"
        Impute NaN and inf values
        """
        if (strategy == 'mean' or strategy == 'median' or strategy == 'most_frequent'):
            imp = SimpleImputer(missing_values=np.nan, strategy=strategy)
            imp.fit(X_train)
            imputed = imp.transform(X_train)
        elif (strategy == 'KNN'):
            imputed = self.KNN_imputer(X_train)
        elif (strategy == 'MI'):
            imputed = self.multi_bayesian_imputation(X_train)
        elif (strategy == 'II'):
            imputed = self.bayesian_imputation(X_train)

        # TODO: Do we need to impute inf values separately?

        return imputed

    # TODO:  attributes that cna be added: n_nearest_features
    def bayesian_imputation(self, data):
        imp = IterativeImputer(missing_values=np.nan, max_iter=10, random_state=self.seed)
        imputed = imp.fit_transform(data)
        return imputed

    #  TODO: hyperparameter iterations - takes forever...
    def multi_bayesian_imputation(self, data, iterations=3):
        imputed_summed = np.zeros(np.shape(data))

        for i in range(iterations):
            print("iteration", i)
            imp = IterativeImputer(missing_values=np.nan, max_iter=10, random_state=self.seed + i,
                                   sample_posterior=True)
            imp_data = imp.fit_transform(data)
            imputed_summed = np.add(imp_data, imputed_summed)
        imputed_average = np.divide(imputed_summed, iterations)
        return imputed_average

    # TODO: How to choose n? can run with couple of values, default is 5
    def KNN_imputer(self, data, n=KNN_IMPUTER_N):
        imputer = KNNImputer(n_neighbors=n, missing_values=np.nan)
        imp_data = imputer.fit_transform(data)
        return imp_data

    def scorer_f(self, estimator, X):  # your own scorer
        return np.mean(estimator.score_samples(X))

    def outlier_detecion_isolation_forest(self, X_train, Y_train, forest_test):
        # grid search for forest to get best params
        model = IsolationForest(
            random_state=self.seed,
            bootstrap=True,
            contamination=OUTLIER_ESTIMATE,
            max_features=10,
            n_estimators=1000,
            n_jobs=-1)
        """
        param_grid = {'n_estimators': [1000, 1500],
                      'max_samples': [10],
                      'contamination': ['auto', 0.01, 0.05, 0.1],
                      'max_features': [10, 15],
                      'bootstrap': [True],
                      'n_jobs': [-1]}

        grid_search = GridSearchCV(model,
                                   param_grid,
                                   scoring=self.scorer_f,
                                   refit=True,
                                   cv=10,
                                   return_train_score=True)

        results = grid_search.fit(X_train, Y_train)
        clf = results.best_estimator_
        """
        clf = model.fit(forest_test)
        clf_predict = clf.predict(X_train)
        X_train_iforest, x_train_GT_iforest = X_train[(clf_predict != -1), :], Y_train[(clf_predict != -1)]
        # best params until now:params {'bootstrap': True, 'contamination': 0.0001, 'max_features': 15, 'max_samples': 10, 'n_estimators': 1000, 'n_jobs': -1}

        # return data without outliers based on isolation forest
        return X_train_iforest, x_train_GT_iforest

    def outlier_detection_local_neighbours(self, X_train, Y_train):
        model = LocalOutlierFactor()
        param_grid = {'n_neighbors': [5, 20, 30],
                      'algorithm': ['auto', 'ball_tree', 'kd_tree'],
                      'leaf_size': [15, 30],
                      'contamination': ['auto']}
        grid_search = GridSearchCV(model,
                                   param_grid,
                                   scoring="neg_mean_squared_error",
                                   refit=True,
                                   cv=10,
                                   return_train_score=True)
        grid_search.fit(X_train, Y_train)
        params = grid_search.best_model.best_params
        clf_predict = LocalOutlierFactor(params).fit_predict(X_train)

        X_train_LO, x_train_GT_LO = X_train[(clf_predict != -1), :], Y_train[(clf_predict != -1)]

        # return data without outliers based on isolation forest
        return X_train_LO, x_train_GT_LO

    def outlier_detection_GM(self, GMM_train, X_train, Y_train):
        '''
        Train an anomaly detector and discard anomalies as introduced in lecture 1.
        '''
        # TODO: Find error why GMM can't have more than two components.
        # Train a Gaussian Mixture Model (GMM) to detect anomalies
        print("Training GMM to detect anomalies...")
        GM = GaussianMixture(n_components=self.gmm_components, init_params='kmeans', random_state=self.seed)
        GM.fit(GMM_train)
        # TODO: For gmm_components > 1, the log score will be positive.... which would imply probabilities > 1!!

        if len(GMM_train[0, :]) > len(X_train[0, :]):
            log_prob_dens = GM.score_samples(GMM_train)
        else:
            log_prob_dens = GM.score_samples(X_train)
        # https://stackoverflow.com/questions/12175404/scikit-learn-gmm-produce-positive-log-probability

        expected_outlier = round(self.est_outlier * len(X_train[:, 0]))

        indices = np.argsort(log_prob_dens)
        indices = indices[expected_outlier - 1:]

        # TODO: Debug this and make sure it does what we expect, I find argwhere a bit confusing
        X_train_final = np.take(X_train, indices, axis=0)
        Y_train_final = np.take(Y_train, indices, axis=0)

        return X_train_final, Y_train_final

    def outlier_det_dbscan(self, X_train, Y_train):
        # reducer = umap.UMAP(n_components=UMAP_COMP, n_neighbors=UMAP_NEIGHBOURS,random_state=self.seed)
        # embedding = reducer.fit_transform(X_train)
        print("Outlier detection with DBSCAN")
        print("Original shape:", X_train.shape)
        embedding = PCA(n_components=2).fit_transform(X_train)

        db = DBSCAN(eps=2.9, min_samples=5).fit(embedding)

        inlier_index = db.core_sample_indices_
        return inlier_index

    def outlier_detection(self, X_train, Y_train, outlier_detector, GMM_train=None):
        if outlier_detector == 'GM':
            return self.outlier_detection_GM(GMM_train, X_train, Y_train)
        if outlier_detector == "IF":
            return self.outlier_detecion_isolation_forest(X_train, Y_train, forest_test=GMM_train)
        if outlier_detector == "LO":
            return self.outlier_detection_local_neighbours(X_train, Y_train)
        if outlier_detector == 'DBS':
            return self.outlier_det_dbscan(X_train, Y_train)

    def find_and_select_features(self, X_train, X_test, Y_train, Feature_Selector,
                                 remove_highly_correlating=True) -> np.ndarray:
        if remove_highly_correlating == USE_PEARSON:  # X_Train needs to be scaled.
            indeces_to_remove = set()
            for i in range(0, len(X_train[0, :])):
                for j in range(i + 1, len(X_train[0, :])):
                    if abs(pearsonr(X_train[:, i], X_train[:, j])[0]) > self.pearson_threshold:
                        indeces_to_remove.add(i)  # Can be refined by deleting the feature with less variance.
                        break
            X_train = np.delete(X_train, list(indeces_to_remove), axis=1)
            X_test = np.delete(X_test, list(indeces_to_remove), axis=1)
            print(str(len(indeces_to_remove)) + ' redundant features removed.')

        if Feature_Selector == 'selectkbest':
            # Train feature selector on the training data
            X_train = self.selectkbest.fit_transform(X_train, np.ravel(Y_train))
            # Also transform the test data
            X_test = self.selectkbest.transform(X_test)
            inlier_index = self.selectkbest.get_support(indices=True)

        if Feature_Selector == 'PCA':
            X_train = self.pca.fit_transform(X_train)
            X_test = self.pca.transform(X_test)

        return X_train, X_test, inlier_index


def main():
    # Load the training dateset and test features
    print('Loading datasets')
    train_features = pd.read_csv('X_train.csv').to_numpy()[:, 1:]
    test_features = pd.read_csv('X_test.csv').to_numpy()[:, 1:]
    train_GT_pre_imp = pd.read_csv('y_train.csv').to_numpy()[:, 1:]

    # Normalize the data
    scaler = StandardScaler()
    train_features_pre_imp = scaler.fit_transform(train_features)
    test_features_pre_imp = scaler.transform(test_features)
    # Remove features with >50% missing values/zeros
    indeces_of_zerovec = [104, 129, 143, 489, 530]
    train_features_pre_imp = np.delete(train_features_pre_imp, indeces_of_zerovec, axis=1)
    test_features_pre_imp = np.delete(test_features_pre_imp, indeces_of_zerovec, axis=1)

    model = RegressionModel()

    # Split the training dataset into training and validation sets for repeated k-fold cross validation
    print('Splitting the training dataset for repeated k-fold cross validation')
    # TODO: In the beginning, we'll only use the repeated k-fold cross validation for the model training in the last step
    # Later, we'll use it for the feature selection step as well (since this should lead to a better generalization performance)
    cross_val = RepeatedKFold(n_splits=model.k_splits, n_repeats=model.k_repeats, random_state=model.seed)

    # Visualize data

    best_imputer = None
    best_feature_selector = None
    best_num_features = None
    best_pearson_threshold = None
    best_outlier_detector = None
    best_gmm_components = None
    best_est_outlier = None
    best_model = None
    best_params = None
    best_accuracy = -1000
    best_outlier_trainset = None

    if IMPUTER_FIXED:
        imputers = IMPUTER
    else:
        imputers = IMPUTERS
    # iterate through imputers
    for imputer in imputers:
        # impute values
        print(f"Imputing values with {imputer} imputation.")
        # use non imputed data for every iteration
        train_features_imp = model.impute(train_features_pre_imp, imputer)
        train_GT_imp = model.impute(train_GT_pre_imp, imputer)
        test_features_imp = model.impute(test_features_pre_imp, imputer)
        

        # iterate through feature selectors
        if FEATURE_FIXED:
            feature_selectors = FEATURE_SELECTOR
        else:
            feature_selectors = FEATURE_SELETCTORS
        for feature_selector in feature_selectors:
            print(f"Selecting features with {feature_selector} feature selector.")
            # select features
            # iterate over number of features

            if NUM_FEATURES_FIXED:
                start = NUM_FEATURES
                end = NUM_FEATURES + 1
                stepsize = 1
            else:
                start = NUM_FEATURES_START
                end = NUM_FEATURES_END
                stepsize = NUM_FEATURES_STEP

            for num_features in range(start, end, stepsize):
                print(f"Selecting {num_features} features.")
                # model.num_features = num_features
                model.selectkbest = SelectKBest(k = num_features)
                # Could be more efficient since we recompute the pearson threshold for every iteration which is pretty useless
                # iterate over pearson threshold

                if PEARSON_FIXED:
                    start = PEARSON_THRESHOLD
                    end = PEARSON_THRESHOLD + 1
                    stepsize = 1
                else:
                    start = PEARSON_THRESHOLD_START
                    end = PEARSON_THRESHOLD_END
                    stepsize = PEARSON_THRESHOLD_STEP

                for pearson_threshold in np.arange(start, end, stepsize):
                    print(f"Selecting features with pearson threshold {pearson_threshold}.")
                    model.pearson_threshold = pearson_threshold
                    _, _, inlier_features = model.find_and_select_features(train_features_imp,
                                                                                           test_features_imp,
                                                                                           train_GT_imp,
                                                                                           feature_selector)

                    train_features_sel_pre_imp = train_features_pre_imp[:, inlier_features]
                    # print(f"Sanity check: {train_features_sel_alt.all() == train_features_sel.all()}")
                    test_features_sel_pre_imp = test_features_pre_imp[:, inlier_features]

                    # Re-impute values
                    print(f"Re-imputing values with {imputer} imputation.")
                    train_features_sel = model.impute(train_features_sel_pre_imp, imputer)
                    test_features_sel = model.impute(test_features_sel_pre_imp, imputer)

                    # iterate through outlier detectors
                    if OUTLIER_FIXED:
                        outliers = OUTLIER_DETECTOR
                    else:
                        outliers = OUTLIER_DETECTORS

                    for outlier in outliers:
                        # detect outliers
                        print(f"Detecting outliers with {outlier} outlier detection.")

                        if outlier == "GM":
                            if GMM_COMPONENTS_FIXED:
                                start = GMM_COMPONENTS
                                end = GMM_COMPONENTS + 1
                                stepsize = 1
                            else:
                                start = GMM_COMPONENTS_START
                                end = GMM_COMPONENTS_END
                                stepsize = GMM_COMPONENTS_STEP
                        else:
                            start = GMM_COMPONENTS
                            end = GMM_COMPONENTS + 1
                            stepsize = 1

                        # iterate through number of GMM components
                        for gmm_components in range(start, end, stepsize):
                            print(f"Choosing {gmm_components} components for GMM.")
                            model.gmm_components = gmm_components

                            if outlier == "GM":
                                if OUTLIER_ESTIMATE_FIXED:
                                    start = OUTLIER_ESTIMATE
                                    end = OUTLIER_ESTIMATE + 1
                                    stepsize = 1
                                else:
                                    start = OUTLIER_ESTIMATE_START
                                    end = OUTLIER_ESTIMATE_END
                                    stepsize = OUTLIER_ESTIMATE_STEP
                            else:
                                start = OUTLIER_ESTIMATE
                                end = OUTLIER_ESTIMATE + 1
                                stepsize = 1

                            # iterate through estimated outliers
                            for est_outlier in np.arange(start, end, stepsize):
                                print(f"Assuming {est_outlier} outlier ratio in data.")
                                model.est_outlier = est_outlier

                                if OUTLIER_TRAINING_SET_FIXED:
                                    outlier_training_sets = OUTLIER_TRAINING_SET
                                else:
                                    outlier_training_sets = OUTLIER_TRAINING_SETS

                                for outlier_training_set in outlier_training_sets:

                                    if outlier_training_set == 'train':
                                        inlier_index = model.outlier_detection(train_features_sel, train_GT_imp,
                                                                               outlier, GMM_train=train_features_sel)
                                    elif outlier_training_set == 'test':
                                        train_features_out, train_GT_out = model.outlier_detection(train_features_sel,
                                                                                                   train_GT_imp,
                                                                                                   outlier,
                                                                                                   GMM_train=test_features_sel)
                                    elif outlier_training_set == 'train_label':
                                        train_features_out, train_GT_out = model.outlier_detection(train_features_sel,
                                                                                                   train_GT_imp,
                                                                                                   outlier,
                                                                                                   GMM_train=np.concatenate(
                                                                                                       (
                                                                                                       train_features_sel,
                                                                                                       train_GT_imp),
                                                                                                       axis=1))
                                    elif outlier_training_set == 'train_test':
                                        train_features_out, train_GT_out = model.outlier_detection(train_features_sel,
                                                                                                   train_GT_imp,
                                                                                                   outlier,
                                                                                                   GMM_train=np.concatenate(
                                                                                                       (
                                                                                                       train_features_sel,
                                                                                                       test_features_sel),
                                                                                                       axis=0))

                                    # train_features_out, train_GT_out = model.outlier_detection(train_features_sel, train_GT_imp, outlier, GMM_train=outlier_training_set)
                                    # print(f"Shape of training features before outlier detection: {train_features_imp.shape}")
                                    # print(f"Shape of training features after outlier detection: {train_features_out.shape}")

                                    # TODO: Repeat the imputation and feature selection step on the original data without outliers
                                    train_features_out = train_features_pre_imp[inlier_index]
                                    train_GT_out = train_GT_pre_imp[inlier_index]

                                    print("Shape after outlier rejection:", train_features_out.shape)

                                    train_features_out_imp = model.impute(train_features_out, "KNN")
                                    train_GT_out_imp = model.impute(train_GT_out, "KNN")

                                    # TODO: Tune second feature selection step
                                    model.num_features = NUM_FEATURES_AFTER_IMP
                                    model.selectkbest = SelectKBest(f_regression, k=model.num_features)


                                    _, _, inlier_features = model.find_and_select_features(train_features_out_imp,
                                                                                           test_features_imp,
                                                                                           train_GT_out_imp,
                                                                                           feature_selector)

                                    train_features_out_sel_pre_imp = train_features_out[:, inlier_features]
                                    # print(f"Sanity check: {train_features_sel_alt.all() == train_features_sel.all()}")
                                    test_features_out_sel_pre_imp = test_features_pre_imp[:, inlier_features]

                                    # Re-impute values
                                    print(f"Re-imputing values with {imputer} imputation after outlier detection.")
                                    train_features_out_sel = model.impute(train_features_out_sel_pre_imp, imputer)
                                    test_features_out_sel = model.impute(test_features_out_sel_pre_imp, imputer)

                                    # iterate through models
                                    if MODEL_FIXED:
                                        models = MODEL
                                    else:
                                        models = MODELS
                                    for modelname in models:
                                        # Initialize the model
                                        # print(f'Initializing {modelname} model')
                                        model = RegressionModel(modelname=modelname)
                                        model.num_features = num_features
                                        model.pearson_threshold = pearson_threshold
                                        model.gmm_components = gmm_components
                                        model.est_outlier = est_outlier

                                        # print(f'Performing a Grid Search over the parameter space of the model')
                                        # Get the appropriate parameter grid for the model
                                        param_grid = model.get_param_grid()
                                        # TODO: Do GridSearch
                                        # TODO: Maybe need to determine all params in the param_grid, not only the ones we want to explore
                                        # TODO: Maybe need to set the scoring function to the R2 score
                                        search = GridSearchCV(model.model, param_grid, cv=cross_val, n_jobs=1,
                                                              scoring='r2', verbose=3)
                                        # Actually perform the repeated k-fold cross validation search
                                        # results will contain the best parameters for the current problem
                                        results = search.fit(train_features_out_sel, train_GT_out_imp)

                                        # Summarize the best performance of the given model over the search
                                        print("------------------------------------------------------")
                                        print(
                                            f"Using {imputer} imputation, {feature_selector} feature selection, \n{outlier} outlier detection, {modelname} model, {outlier_training_set} \noutlier training set with {num_features} features, pearson threshold \n{pearson_threshold}, {gmm_components} GMM components and {est_outlier} \noutliers.")
                                        print('Best Mean Accuracy: %.3f' % results.best_score_)
                                        print('Best Config: %s' % results.best_params_)
                                        print("------------------------------------------------------")
                                        # # Summarize search over all parameters
                                        means = results.cv_results_['mean_test_score']
                                        params = results.cv_results_['params']
                                        for mean, param in zip(means, params):
                                            print(">%.3f with: %r" % (mean, param))

                                        # Check if new best model has been found and save it accordingly
                                        if results.best_score_ > best_accuracy:
                                            # TODO: Check, that we really save the best model in our Regressor Class correctly
                                            # Save the best model in our Regressor
                                            model.model = results.best_estimator_

                                            best_accuracy = results.best_score_
                                            best_imputer = imputer
                                            best_feature_selector = feature_selector
                                            best_num_features = model.num_features
                                            best_pearson_threshold = model.pearson_threshold
                                            best_outlier_detector = outlier
                                            best_gmm_components = model.gmm_components
                                            best_est_outlier = model.est_outlier
                                            best_model = model
                                            best_params = results.best_params_
                                            best_outlier_training_set = outlier_training_set
                                            print(
                                                f"{modelname} new best model with accuracy {best_accuracy} and parameters {results.best_params_}")

                                            # Save configuration
                                            fields = [best_accuracy, best_imputer, best_feature_selector,
                                                      best_num_features, best_pearson_threshold,
                                                      best_outlier_detector, best_gmm_components, best_est_outlier,
                                                      model.k_splits,
                                                      model.k_repeats, modelname, best_params,
                                                      best_outlier_training_set]
                                            with open(r'experiment.csv', 'a') as f:
                                                writer = csv.writer(f)
                                                writer.writerow(fields)

    print(f'--------------------------------------------')
    print(f'Best found model: {best_model.modelname}')
    print(f'Model parameters: {best_params}')
    print(f'Best found imputer: {best_imputer}')
    print(f'Best found feature selector: {best_feature_selector}')
    print(f'With {best_num_features} features')
    print(f'And pearson threshold of {best_pearson_threshold}')
    print(f'Best found outlier detector: {best_outlier_detector}')
    print(f'With {best_gmm_components} GMM components')
    print(f'And {best_est_outlier} estimated outliers')
    print(f'Best found accuracy: {best_accuracy}')
    print(f'--------------------------------------------')

    # Train the best model on all the data now and predict on the test set
    print("Training the best model on all the training data and predicting on the test set")
    # Imputation
    print(best_model.model.get_params())
    train_features_imp = best_model.impute(train_features_pre_imp, best_imputer)
    train_GT_imp = best_model.impute(train_GT_pre_imp, best_imputer)
    test_features_imp = best_model.impute(test_features_pre_imp, best_imputer)
    
    # Feature selection
    best_model.selectkbest = SelectKBest(f_regression, k=NUM_FEATURES)
    # TODO: At the moment, we only train the feature selection on the training data -> maybe change it to also train on the test data, but be careful of data leakage/overfitting

    _, _, inlier_features = best_model.find_and_select_features(train_features_imp,
                                                            test_features_imp,
                                                            train_GT_imp,
                                                            best_feature_selector)

    train_features_sel_pre_imp = train_features_pre_imp[:, inlier_features]
    # print(f"Sanity check: {train_features_sel_alt.all() == train_features_sel.all()}")
    test_features_sel_pre_imp = test_features_pre_imp[:, inlier_features]

    # Re-impute values
    print(f"Re-imputing values with {imputer} imputation.")
    train_features_sel = model.impute(train_features_sel_pre_imp, best_imputer)
    test_features_sel = model.impute(test_features_sel_pre_imp, best_imputer)

    # Outlier detection
    if best_outlier_training_set == 'train':
        inlier_index = best_model.outlier_detection(train_features_sel, train_GT_imp, outlier, GMM_train=train_features_sel)
    elif best_outlier_training_set == 'test':
        train_features_out, train_GT_out = best_model.outlier_detection(train_features_sel, train_GT_imp, outlier,
                                                                   GMM_train=test_features_sel)
    elif best_outlier_training_set == 'train_label':
        train_features_out, train_GT_out = best_model.outlier_detection(train_features_sel, train_GT_imp, outlier,
                                                                   GMM_train=np.concatenate(
                                                                       (train_features_sel, train_GT_imp), axis=1))
    elif best_outlier_training_set == 'train_test':
        train_features_out, train_GT_out = best_model.outlier_detection(train_features_sel, train_GT_imp, outlier,
                                                                   GMM_train=np.concatenate(
                                                                       (train_features_sel, test_features_sel), axis=0))

    train_features_out = train_features_pre_imp[inlier_index]
    train_GT_out = train_GT_pre_imp[inlier_index]

    print("Shape after outlier rejection:", train_features_out.shape)

    train_features_out_imp = best_model.impute(train_features_out, "KNN")
    train_GT_out_imp = best_model.impute(train_GT_out, "KNN")

    # TODO: Tune second feature selection step
    best_model.num_features = NUM_FEATURES_AFTER_IMP
    best_model.selectkbest = SelectKBest(f_regression, k=best_model.num_features)

    _, _, inlier_features = best_model.find_and_select_features(train_features_out_imp,
                                                            test_features_imp,
                                                            train_GT_out_imp,
                                                            best_feature_selector)

    train_features_out_sel_pre_imp = train_features_out[:, inlier_features]
    # print(f"Sanity check: {train_features_sel_alt.all() == train_features_sel.all()}")
    test_features_out_sel_pre_imp = test_features_pre_imp[:, inlier_features]

    # Re-impute values
    print(f"Re-imputing values with {imputer} imputation.")
    train_features_out_sel = model.impute(train_features_out_sel_pre_imp, imputer)
    test_features_out_sel = model.impute(test_features_out_sel_pre_imp, imputer)

    # Fit a regression model (Boosted Decision Tree, XGBoost for the moment)
    # Instructions: https://www.geeksforgeeks.org/xgboost-for-regression/
    print('Fitting model')
    best_model.fit_model(train_features_out_sel, train_GT_out_imp)
    training_r2 = best_model.accuracy(best_model.predict(train_features_out_sel), train_GT_out_imp)
    print(f'Overall training r2 score: {training_r2}')

    # Predict on the test features
    print('Predicting on test features')
    predictions = best_model.predict(test_features_out_sel)

    # TODO: If there is a big discrepancy between the validation and test set accuracy, the most likely culprit is the outlier detection, since the
    # test data has NO outliers in it. So with a good outlier detection, the two sets should be "drawn from the same distribution" and should generalize

    # Save the predictions to a csv file
    print('Saving predictions to csv file')
    idx = np.arange(0.0, len(predictions))
    # Finally got the got damn format right
    pd.Series(predictions, idx).map(lambda x: '%1.14f' % x).to_csv('pred_test.csv', index_label='id', header=['y'],
                                                                   float_format='%1.1f')
    # pd.Series(predictions, idx).map(lambda x: '%1.1f' % x).to_csv('predictions.csv', index_label = 'id', header = ['y'], float_format='%1.14f')
    # TODO: Due to hack need to change csv header by hand


if __name__ == "__main__":
    main()

