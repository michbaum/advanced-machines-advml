#%%
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import umap
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN

# %%

#%%

def main():
    # Load the data
    train_features = pd.read_csv('X_train.csv')
    test_features = pd.read_csv('X_test.csv')
    train_GT_pre_imp = pd.read_csv('y_train.csv')

    # Visualize the data
    # print(train_features.info(max_cols=850))
    # print(test_features.info(max_cols=850))
    # print(train_GT_pre_imp.info())

    # Most columns don't have that many missing valuse, never more than 10%

    # print(train_features.describe())
    # print(test_features.describe())
    # print(train_GT_pre_imp.describe())

    # Saerch the features with the most NaNs
    print(train_features.isnull().sum().sort_values(ascending=False)[0:20])

    # Search the features with the most zeros
    print(train_features.isin([0]).sum().sort_values(ascending=False)[0:10])

    # Check the distribution of the target variable with lots of zeros
    # print(train_features['x143'].describe())
    # It sucks, delete it

    train_features = train_features.to_numpy()[:, 1:]
    indeces_of_zerovec = [104, 129, 143, 489, 530]
    train_features= np.delete(train_features, indeces_of_zerovec,axis=1)

    # Visualize the data with UMAP after imputation
    scaler = StandardScaler()
    train_features_pre_imp = scaler.fit_transform(train_features)
    imp = SimpleImputer(missing_values = np.nan, strategy = "mean")
    imp.fit(train_features_pre_imp)
    imputed = imp.transform(train_features_pre_imp)
    
    # dim_reduced_train_features = umap.UMAP(n_neighbors=2).fit_transform(imputed)
    dim_reduced_train_features = PCA(n_components=2).fit_transform(imputed)
    plt.scatter(dim_reduced_train_features[:, 0], dim_reduced_train_features[:, 1])
    plt.show()

    # Apply DBSCAN to the data
    db = DBSCAN(eps=2.8, min_samples=5).fit(dim_reduced_train_features)
    plt.scatter(dim_reduced_train_features[:, 0], dim_reduced_train_features[:, 1], c = db.labels_)
    plt.show()

    dim_reduced_train_features = dim_reduced_train_features[db.core_sample_indices_]

    plt.scatter(dim_reduced_train_features[:, 0], dim_reduced_train_features[:, 1])
    plt.show()

    plt.scatter(db.components_[:, 0], db.components_[:, 1])
    plt.show()


if __name__ == "__main__":
    main()
# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%
