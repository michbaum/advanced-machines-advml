# Advanced Machines AdvML



## Projects

This Gitlab project is created to keep an overview and structure the projects for Advanced Machine Learning (HS22).
The project website for submission and task downloads can be found here: [AdvML tasks] (https://aml.ise.inf.ethz.ch/)

## Useful git commands

- [ ] git clone (initialise project)
- [ ] git pull (get latest version of your branch/master branch)
- [ ] git add -A
- [ ] git commit -m "comment"
- [ ] git push 
- [ ] git checkout -b branchname (create a branch with the given name and move to it)
- [ ] git checkout branchname (move to an existing branch)


